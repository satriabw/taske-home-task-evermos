from random import randrange
from typing import List

from config import db, ma
from models import BallContainer, Product

class FindKeyService():
    def __init__(self):
        self.map = self._get_map()

    def _get_map(self) -> List[List[str]]:
        return [
            ['#', '#', '#', '#', '#', '#' , '#', '#'],
            ['#', '.', '.', '.', '.', '.' , '.', '#'],
            ['#', '.', '#', '#', '#', '.' , '.', '#'],
            ['#', '.', '.', '.', '#', '.' , '#', '#'],
            ['#', 'X', '#', '.', '.', '.', '.' , '#'],
            ['#', '#', '#', '#', '#', '#' , '#', '#'],
        ]
    

    def countNumberOfDots(self) -> int:
        self.fillMap()

        height = len(self.map)
        width = len(self.map[0])
        count = -1 # Minus starting point

        for i in range(height):
            for j in range(width):
                if self.map[i][j] == 'v':
                    count += 1
        return count

    def fillMap(self):
        height = len(self.map)
        width = len(self.map[0])

        for i in range(height):
            for j in range(width):
                if self.map[i][j] == 'X':
                    self._fillMapHelper(self.map, i, j, height, width)
                    return
    
    def _fillMapHelper(self, map: List[List[str]], x: int, y: int, height:int, width: int):
        if self._isStuck(map, x, y, height, width):
            return
        
        map[x][y] = 'v'

        self._fillMapHelper(map, x-1, y, height, width) # to the north
        self._fillMapHelper(map, x, y+1, height, width) # to the east
        self._fillMapHelper(map, x+1, y, height, width) # to the south

        return

    def _isStuck(self, map: List[List[str]], x: int, y: int, height: int, width: int) -> bool:
        if x < 0 or x >= height or y < 0 or y >= width or map[x][y] == 'v' or map[x][y] == '#':
            return True
        return False


class BallContainerService:
    def add_new_ball_container(self, max_capacity: int):
        ball_container = BallContainer(maximum_capacity=max_capacity)
        db.session.add(ball_container)
        db.session.commit()

    def get_ball_container(self, container_id: int) -> BallContainer:
        ball_container = BallContainer.query.filter_by(id=container_id).first()

        return ball_container

    def get_verified_container(self) -> List[BallContainer]:
        ball_containers = BallContainer.query.filter_by(is_verified=True)

        return ball_containers

    def add_ball_to_container(self, container_id: int) -> BallContainer:
        ball_container = self.get_ball_container(container_id)

        if not ball_container or ball_container.is_verified:
            return
        
        ball_container.num_of_balls += 1
        ball_container.is_verified = ball_container.num_of_balls == ball_container.maximum_capacity

        db.session.commit()

        return ball_container
    
    def remove_ball_from_container(self, container_id: int) -> BallContainer:
        ball_container = self.get_ball_container(container_id)

        if not ball_container or ball_container.is_verified or ball_container.num_of_balls == 0:
            return
        
        ball_container.num_of_balls -= 1
        db.session.commit()

        return ball_container
    
    def get_all_ball_containers(self) -> List[BallContainer]:
        return BallContainer.query.all()


class ProductService:
    def buy_product(self, product_id: int) -> Product:
        product = Product.query.filter_by(id=product_id).with_for_update().one()

        if product.quantity == 0:
            return
        
        product.quantity -= 1
        db.session.commit()

        return product
        

find_key_service = FindKeyService()
ball_container_service = BallContainerService()
product_service = ProductService()

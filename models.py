from config import db, ma

class BallContainer(db.Model):
    __tablename__ = 'ball_container'

    id = db.Column(db.Integer, primary_key=True)

    num_of_balls = db.Column(db.Integer, default=0)
    maximum_capacity = db.Column(db.Integer, default=0)

    is_verified = db.Column(db.Boolean, default=False)


class Product(db.Model):
    __tablename__ = 'product'

    id = db.Column(db.Integer, primary_key=True)

    quantity = db.Column(db.Integer, default=0)


class BallContainerSchema(ma.Schema):
    class Meta:
        model = BallContainer
        fields = ('id', 'num_of_balls', 'maximum_capacity', 'is_verified')
        sqla_session = db.session


class ProductSchema(ma.Schema):
    class Meta:
        model = Product
        fields = ('id', 'quantity')
        sqla_session = db.session

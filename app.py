import os

from flask import abort, jsonify, request

from config import app
from models import BallContainerSchema, ProductSchema
from services import ball_container_service, find_key_service, product_service


@app.route("/ball-containers")
def get_ball_containers():
    ball_containers = ball_container_service.get_all_ball_containers()
    ball_container_schema = BallContainerSchema(many=True)
    data = ball_container_schema.dump(ball_containers)

    return jsonify(data)

@app.route("/ball-containers", methods=["POST"])
def create_ball_container():
    maximum_capacity = request.json.get('maximum_capacity')

    if not maximum_capacity:
       abort(400)

    ball_container_service.add_new_ball_container(maximum_capacity)
    return jsonify({'success': True})

@app.route("/ball-containers/<id>/add", methods=["POST"])
def add_ball_to_container(id):
    ball_container = ball_container_service.add_ball_to_container(id)

    if not ball_container:
        return jsonify({'message': 'Failed to add ball to container'})

    ball_container_schema = BallContainerSchema()
    data = ball_container_schema.dump(ball_container)

    return jsonify(data)

@app.route("/ball-containers/<id>/remove", methods=["POST"])
def remove_ball_to_container(id):
    ball_container = ball_container_service.remove_ball_from_container(id)
    
    if not ball_container:
        return jsonify({'message': 'Failed to remove ball from container'})

    ball_container_schema = BallContainerSchema()
    data = ball_container_schema.dump(ball_container)

    return jsonify(data)

@app.route("/products/<id>/buy", methods=["POST"])
def buy_product(id):
    product = product_service.buy_product(id)

    if not product:
        return jsonify({'message': 'Failed to buy product'})
    
    product_schema = ProductSchema()
    data = product_schema.dump(product)
    
    return jsonify(data)


@app.route("/find_key")
def find_key():
    num_of_dots = find_key_service.countNumberOfDots()

    return f'The number of dots is {num_of_dots}'
  
if __name__ == "__main__":
    app.run(debug=True)

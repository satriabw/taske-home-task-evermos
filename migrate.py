import os
from config import db
from models import BallContainer, Product

if os.path.exists('task.db'):
    os.remove('task.db')

db.create_all()

def generate_product():
    product = Product(quantity=10)
    db.session.add(product)
    db.session.commit()

generate_product()

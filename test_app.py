import sys, os
myPath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, myPath + '/../')

import unittest
import json

from config import app, db
from models import BallContainer





class TestBallContainer(unittest.TestCase):

    def setUp(self):
        self.client = app.test_client() 

        db.create_all()
        self.ball_containers = BallContainer(maximum_capacity=1)
        db.session.add(self.ball_containers)
        db.session.commit()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_get_user(self):
        self.client.post('/ball-containers', data=dict(maximum_capacity=1))
        container = self.client.post('/ball-containers/1/add')
        self.assertEqual(container.is_verified, True)


if __name__ == '__main__':
    unittest.main()